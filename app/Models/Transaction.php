<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\CardNumber;
class Transaction extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function card_number()
    {
        return $this->belongsTo(CardNumber::class);
    }
}
