<?php
namespace App\Message;
use App\InterFace\MessageInterface;
use App\Models\CardNumber;

class MessageFormat implements MessageInterface{

    public function success_origin_card($origin_card,$destination_card,$amount){
        return " پرداخت شما از شماره کارت".$origin_card.
        "به شماره حساب ".$destination_card."به مبلغ".$amount."موفقیت امیز بود";
    }
    public function success_destination_card($origin_card,$destination_card,$amount){
     return "مبلغ ".$amount."از شماره کارت".$origin_card."به شماره کارت شما:".$destination_card
     ."انتقال یافت";
    }
    public function fail_origin_card($origin_card,$destination_card,$amount){
      return  " پرداخت شما از شماره کارت".$origin_card.
        "به شماره حساب ".$destination_card."به مبلغ".$amount. "ناموفق بود";
    }
    public function fail_destination_card($origin_card,$destination_card,$amount){
        return "انتقال مبلغ".$amount."از شماره کارت".$origin_card."به شماره کارت:".$destination_card."شما ناموفق بود";
    }

}

?>
