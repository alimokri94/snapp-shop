<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\AccountNumber;
use App\Models\Transaction;
class CardNumber extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function account_number()
    {
        return $this->belongsTo(AccountNumber::class);
    }
    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }
}
