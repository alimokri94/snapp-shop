<?php
namespace App\InterFace;

use App\Models\CardNumber;

interface MessageInterface{
    public function success_origin_card($origin_card,$destination_card,$amount);
    public function success_destination_card($origin_card,$destination_card,$amount);
    public function fail_origin_card($origin_card,$destination_card,$amount);
    public function fail_destination_card($origin_card,$destination_card,$amount);

}
?>
