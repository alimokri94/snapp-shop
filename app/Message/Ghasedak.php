<?php
namespace App\Message;
use App\InterFace\MessageInterface;
use App\InterFace\MessageSenderInterface;
use Illuminate\Support\Facades\Log;
class Ghasedak{
    public function __construct(private MessageInterface $message)
    {
    }
    public function send($origin_card,$destination_card,$amount,$type){
        if($type==="success"){
            $origin_card_message= $this->message->success_origin_card($origin_card,$destination_card,$amount);
            $destination_card_message=$this->message->success_destination_card($origin_card,$destination_card,$amount);
        }else{
            $origin_card_message= $this->message->fail_origin_card($origin_card,$destination_card,$amount);
            $destination_card_message=$this->message->fail_destination_card($origin_card,$destination_card,$amount);
        }
        Log::info("قاصذک:".$origin_card_message);
        Log::info("قاصدک:".$destination_card_message);


    }
}

?>
