<?php
namespace App\Repository;

use App\Models\CardNumber;
use Illuminate\Support\Facades\DB;
use App\Models\Wage;
use App\Models\Transaction;

class TransformMoneyRepository{


    public function transaction($origin_card, $destination_card, $amount,$fee_amount)
    {

        DB::transaction(function () use ($origin_card, $destination_card, $amount,$fee_amount) {
            $origin_card->decrement('amount', $amount + $fee_amount);
            $destination_card->increment("amount", $amount);
            $transaction = Transaction::create([
                "card_number_id" => $origin_card->id,
                "status" => "success",
                "destination_card_number" => $destination_card->card_number,
                "amount" => floatval($amount),
            ]);
            Wage::create(["transaction_id" => $transaction->id, "fee" => $fee_amount]);
        });

    }
    public function find_card_number($card_number){
        return CardNumber::where("card_number", $card_number)->first();
    }
    public function doesnt_exist_Card_number($card_number){
        return CardNumber::where("card_number", $card_number)->doesntExist();
    }
    public function fail_transaction($origin_card, $destination_card, $amount){
        $transaction = Transaction::create([
            "card_number_id" => $origin_card->id,
            "status" => "fail",
            "destination_card_number" => $destination_card->card_number,
            "amount" => floatval($amount),
        ]);
    }
}
