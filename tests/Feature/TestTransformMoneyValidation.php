<?php

namespace Tests\Feature;


use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class TestTransformMoneyValidation extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_card_number_less_then_16()
    {

        $response = $this->post('/api/transfer-money',["origin_card"=>"12","destination_card"=>"14","amount"=>"10000"]);

        $response->assertStatus(200)->assertJson([
            "error"=>["origin_card"=>"The origin card must be 16 digits."
            ,"destination_card"=>"The destination card must be 16 digits."
            ,"amount"=>""]
        ]);
    }

    public function test_amount_less_then_1000()
    {

        $response = $this->post('/api/transfer-money',["origin_card"=>"122",
        "destination_card"=>"14","amount"=>"10"]);

        $response->assertStatus(200)->assertJson([
            "error"=>["origin_card"=>"The origin card must be 16 digits.",
            "destination_card"=>"The destination card must be 16 digits.",
            "amount"=>"The amount must be at least 1000."]
        ]);
    }
    public function test_reauired(){
        $response = $this->post('/api/transfer-money',["origin_card"=>"","destination_card"=>"14",
        "amount"=>"10"]);
        $response->assertStatus(200)->assertJson([
            "error"=>["origin_card"=>"The origin card field is required.",
            "destination_card"=>"The destination card must be 16 digits.",
            "amount"=>"The amount must be at least 1000."]
        ]);

        $response = $this->post('/api/transfer-money',["origin_card"=>"122","destination_card"=>"","amount"=>"10000"]);
        $response->assertStatus(200)->assertJson([
            "error"=>["origin_card"=>"The origin card must be 16 digits.","destination_card"=>"The destination card field is required.","amount"=>""]
        ]);

        $response = $this->post('/api/transfer-money',["origin_card"=>"122","destination_card"=>"14"
        ,"amount"=>""]);
        $response->assertStatus(200)->assertJson([
            "error"=>["origin_card"=>"The origin card must be 16 digits."
            ,"destination_card"=>"The destination card must be 16 digits."
            ,"amount"=>"The amount field is required."]
        ]);
    }
}
