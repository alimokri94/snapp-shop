<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\AccountNumber;
use App\Models\CardNumber;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $user1=\App\Models\User::factory()->create(["phone_number"=>"09113079381"]);
        $account_number=AccountNumber::factory(1)->for($user1)->create();
         CardNumber::factory(1)->create(["card_number"=>"6221066221061234","amount"=>"50000000"]);
         CardNumber::factory(1)->create(["card_number"=>"6221066221061256","amount"=>"1000"]);

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);
    }
}
