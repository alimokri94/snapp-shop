<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class CardNumberValidate implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $prefix_card=["627412","627381","505785",
        "622106","639194","627884"
        ,"639347","502229","636214",
        "627353","502908","627648",
        "207177","636949","502938"
        ,"589463","621986","589210",
        "639607","639346","502806",
        "603769","627961","606373",
        "639599","627488","502910",
        "603770","639217","505416",
        "636795","628023","610433",
        "991975","603799","639370",
        "627760","628157","505801"];
       return in_array(substr($value,0,6),$prefix_card);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'شماره کارت درست نیست';
    }
}
