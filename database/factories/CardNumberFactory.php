<?php

namespace Database\Factories;

use App\Models\AccountNumber;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\CardNumber>
 */
class CardNumberFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    private $prefix_card=["627412","627381","505785",
    "622106","639194","627884"
    ,"639347","502229","636214",
    "627353","502908","627648",
    "207177","636949","502938"
    ,"589463","621986","589210",
    "639607","639346","502806",
    "603769","627961","606373",
    "639599","627488","502910",
    "603770","639217","505416",
    "636795","628023","610433",
    "991975","603799","639370",
    "627760","628157","505801"];
    public function definition()
    {
        return [
            "account_number_id"=>AccountNumber::factory(),
            "card_number"=>$this->generate_fake_card_number(),
            "amount"=>10000
        ];
    }
    private function generate_fake_card_number(){
        $numbers=range(0,9);
        $fake_number="";
        for($i=0;$i<10;$i++){
        $fake_number.=$numbers[mt_rand(0,9)];
        }
        return mt_rand(0, $this->prefix_card[count($this->prefix_card)-1])
        .$fake_number;
    }
}
