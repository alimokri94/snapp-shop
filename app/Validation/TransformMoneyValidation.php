<?php
namespace App\Validation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Rules\CardNumberValidate;
use App\Models\CardNumber;
use App\Repository\TransformMoneyRepository;

class TransformMoneyValidation{

    public function validator(Request $request)
    {
        $validator_all_error=[];
        $validator = Validator::make($request->all(), [
            'origin_card' => ['required', "digits:16", new CardNumberValidate],
            'destination_card' => ["required", "digits:16", new CardNumberValidate],
            'amount' => ["required", "integer", "min:1000", "max:50000000"]
        ]);

        if($validator->fails()){
            $errors = $validator->errors();
        $validator_all_error = [
            "origin_card" => $errors->first('origin_card'), "destination_card" => $errors->first('destination_card'),
            'amount' => $errors->first("amount")
        ];
            return $validator_all_error;
        }
        $repository=new TransformMoneyRepository();
        if ($repository->doesnt_exist_Card_number($request->origin_card)) {
            $validator_all_error["origin_card"] = "کارت مبدا در سیستم موجود نمی باشد";
        }
        if ($repository->doesnt_exist_Card_number($request->destination_card)) {
            $validator_all_error["destination_card"] = "کارت مقصد در سیستم موجود نمی باشد";
        }

        return $validator_all_error;
    }

}
?>
