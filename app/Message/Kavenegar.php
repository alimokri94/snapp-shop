<?php
namespace App\Message;
use App\InterFace\MessageInterface;
use App\InterFace\MessageSenderInterface;
use Illuminate\Support\Facades\Log;
class Kavenegar implements MessageSenderInterface{
    public function __construct(private MessageInterface $message)
    {
    }
    public function send($origin_card,$destination_card,$amount,$type){
        if($type==="success"){
            $origin_card_message= $this->message->success_origin_card($origin_card,$destination_card,$amount);
            $destination_card_message=$this->message->success_destination_card($origin_card,$destination_card,$amount);
        }else{
            $origin_card_message= $this->message->fail_origin_card($origin_card,$destination_card,$amount);
            $destination_card_message=$this->message->fail_destination_card($origin_card,$destination_card,$amount);
        }
        Log::info("کاوه نگار:".$origin_card_message);
        Log::info("کاوه نگار:".$destination_card_message);


    }

}
