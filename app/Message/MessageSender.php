<?php
namespace App\Message;
use App\InterFace\MessageSenderInterface;

class MessageSender{
    private $message_sender_strategy;
    public function __construct(MessageSenderInterface $message_sender)
    {
        $this->message_sender_strategy=$message_sender;
    }
    public function send($origin_card,$destination_card,$amount,$type){
       return $this->message_sender_strategy->send($origin_card,$destination_card,$amount,$type);

    }
}

?>
