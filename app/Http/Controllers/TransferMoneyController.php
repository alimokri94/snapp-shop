<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Message\MessageSender;
use App\Message\Kavenegar;
use App\Message\MessageFormat;
use App\Repository\TransformMoneyRepository;
use App\Validation\TransformMoneyValidation;

class TransferMoneyController extends Controller
{
    private $fee_amount = 500;
    public function transfer_money(Request $request)
    {
        $message=new MessageSender(new Kavenegar(new MessageFormat));

        $repository=new TransformMoneyRepository($this->fee_amount);
        $origin_card=$repository->find_card_number($request->origin_card);
        $destination_card=$repository->find_card_number($request->destination_card);

        $validation=new TransformMoneyValidation();
        $validator_all_error = $validation->validator($request);

        if (count($validator_all_error)>0) {
            return response()->json(["error" => $validator_all_error]);
        }
        if($this->is_low_inventory($origin_card,$request)){
            $message->send($origin_card->card_number,$destination_card->card_number,$request->amount,"fail");
            $repository->fail_transaction($origin_card, $destination_card, $request->amount);
            return response()->json(["error"=>"موجودی کارت کم است"]);
        }
        $repository->transaction($origin_card, $destination_card, $request->amount,$this->fee_amount);
        $message->send($origin_card->card_number,$destination_card->card_number,$request->amount,"success");
        return response()->json(["success" => "پرداخت شما موفقیت امیز بود"]);
    }


    private function is_low_inventory($origin_card,$request){
        return floatval($origin_card->amount) < floatval($request->amount) + $this->fee_amount;
    }

}
